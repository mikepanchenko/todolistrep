//
//  CoreDataManager.swift
//  ezToDoList
//
//  Created by Pro on 18.10.2019.
//  Copyright © 2019 Pro. All rights reserved.
//

import UIKit
import CoreData
class CoreDataManager {
    private let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    private var fetchedRC: NSFetchedResultsController<Task>!
    var tasks: [Task] = []
    weak var delegate: ReloadDataDelegate?
    
    func downloadData() {
        let request = Task.fetchRequest() as NSFetchRequest<Task>
             let sort = NSSortDescriptor(key: #keyPath(Task.finishdate), ascending: true)
             request.sortDescriptors = [sort]
             do {
               fetchedRC = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
               try fetchedRC.performFetch()
             } catch let error as NSError {
               print("Could not fetch. \(error), \(error.userInfo)")
             }
        tasks = fetchedRC.fetchedObjects ?? []
    }
    func saveData() {
        appDelegate.saveContext()
    }
    
    func createID() -> Int64 {
        return tasks.map{$0.id}.reduce(1,+)
    }
    
    func getTask(with id:Int64) -> Task? {
        var needtask:Task?
        tasks.forEach { (task) in
            if task.id == id {
                 needtask = task
            }
        }
        return needtask
    }
}
let coreDataManager = CoreDataManager()

