//
//  TableViewController.swift
//  ezToDoList
//
//  Created by Pro on 17.10.2019.
//  Copyright © 2019 Pro. All rights reserved.
//

import UIKit
import CoreData

protocol ReloadDataDelegate: class{
    func reload()
}

class TableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        coreDataManager.delegate = self
        tableView.register(UINib(nibName: "MainTableViewCell", bundle: nil), forCellReuseIdentifier: "MainTableViewCell")
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return coreDataManager.tasks.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath) as! MainTableViewCell
        cell.setTask(task: coreDataManager.tasks[indexPath.row])
        cell.nameLbl.text = coreDataManager.tasks[indexPath.row].name
        let dateform = DateFormatter()
        dateform.dateStyle = .medium
        cell.planDateLbl.text = dateform.string(from: coreDataManager.tasks[indexPath.row].createdate!)

        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as? DetailViewController
               {
                vc.navigationItem.title = coreDataManager.tasks[indexPath.row].name
                vc.task = coreDataManager.tasks[indexPath.row]
                    show(vc, sender: nil)
               }
    }

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [String(coreDataManager.tasks[indexPath.row].id)])
            coreDataManager.context.delete(coreDataManager.tasks[indexPath.row])
            coreDataManager.tasks.remove(at: indexPath.row)
            coreDataManager.saveData()
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    
    // MARK: Action funcs
    
    @IBAction func addAction(_ sender: Any) {
      
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddTaskViewController") as? AddTaskViewController
        {
            vc.reloaddelegate = self
            present(vc, animated: true, completion: nil)
        }
    }

}


//MARK: - Extension

extension TableViewController: ReloadDataDelegate{
    func reload() {
        tableView.reloadData()
    }
}
