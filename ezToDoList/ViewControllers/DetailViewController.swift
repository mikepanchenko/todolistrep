//
//  ViewController.swift
//  ezToDoList
//
//  Created by Pro on 17.10.2019.
//  Copyright © 2019 Pro. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    
    //MARK: - Prop
   
    weak var task:Task?
     
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    
    
    //MARK: - LifeCycle Method

    override func viewDidLoad() {
        super.viewDidLoad()
        taskDetailUpgrade(task!)
        // Do any additional setup after loading the view.
    }

    
    //MARK: - Private Method
  
    private func taskDetailUpgrade(_ task:Task) {
        let dataform = DateFormatter()

        dataform.dateFormat = "yyyy-MM-dd HH:mm"
        startDateLbl.text = "Create date: \(dataform.string(from: task.createdate!))"
        endDateLbl.text = "Finish date: \(dataform.string(from: task.finishdate!))"
        descriptionLbl.text = task.taskdescription
        descriptionLbl.sizeToFit()
    }

}

