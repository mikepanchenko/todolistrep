//
//  AddTaskViewController.swift
//  ezToDoList
//
//  Created by Pro on 18.10.2019.
//  Copyright © 2019 Pro. All rights reserved.
//

import UIKit
import CoreData

class AddTaskViewController: UIViewController  {
    
    
    //MARK: - Prop
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var planDatePicker: UIDatePicker!
    
    var reloaddelegate: ReloadDataDelegate?
    
    
     //MARK: - LifeCycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        planDatePicker.minimumDate = Date()
        nameTF.delegate = self
        descriptionTV.layer.borderWidth = 1.0
        descriptionTV.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        descriptionTV.layer.cornerRadius = 8.0
        // Do any additional setup after loading the view.
    }
    
    
    // MARK: - IBAction method
    
    @IBAction func saveBtnAction(_ sender: Any) {
        if nameTF.text!.isEmpty {
            
        }else{
            
            let task = Task(context: coreDataManager.context)
            task.id = coreDataManager.createID()
            task.name = nameTF.text
            task.finishdate = planDatePicker.date
            task.createdate = Date()
            task.taskdescription = descriptionTV.text
            coreDataManager.tasks.append(task)
            reloaddelegate?.reload()
            self.dismiss(animated: true, completion: nil)
            if (task.finishdate?.compare(task.createdate!).rawValue ?? 0 > 0){
                print("hi")
                setNotification(task: task)
            }
            coreDataManager.saveData()
        }
    }
    
    @IBAction func doneAction(_ sender: Any) {
        descriptionTV.resignFirstResponder()
        nameTF.resignFirstResponder()
    }
    
    
    // MARK: - Private method
    
    private func setNotification(task:Task){
        
        let content = UNMutableNotificationContent()
        content.title = task.name ?? ""
        content.body = task.taskdescription ?? ""
        content.sound = .default
        content.categoryIdentifier = "alarm"
        content.userInfo = ["TASK_ID":task.id]
        //Set the trigger of the notification -- here a timer.
        guard let createDate = task.createdate,
              let finishDate = task.finishdate else {return}
        
        let trigger = UNTimeIntervalNotificationTrigger(
            timeInterval: finishDate.timeIntervalSince(createDate),
            repeats: false)
        
        //Set the request for the notification from the above
        let request = UNNotificationRequest(
            identifier: String(task.id),
            content: content,
            trigger: trigger
        )

        //Add the notification to the currnet notification center
        UNUserNotificationCenter.current().add(
            request)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


//MARK: - Extension

extension AddTaskViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        doneAction(self)
        return true
    }
}

