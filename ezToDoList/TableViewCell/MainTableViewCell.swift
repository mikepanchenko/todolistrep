//
//  MainTableViewCell.swift
//  ezToDoList
//
//  Created by Pro on 17.10.2019.
//  Copyright © 2019 Pro. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var planDateLbl: UILabel!
    @IBOutlet weak var doneButton: UIButton!
    
    
    var task: Task?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func doneAction(_ sender: UIButton) {
        guard let task = task else {return}
        if !task.isDone{
            UIView.transition(with: sender, duration: 0.5, options: .transitionFlipFromTop, animations: nil, completion: nil)
            sender.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
            task.isDone = true
            coreDataManager.saveData()
        }else{
            UIView.transition(with: sender, duration: 0.5, options: .transitionFlipFromBottom, animations: nil, completion: nil)
            sender.setImage(#imageLiteral(resourceName: "UnDone"), for: .normal)
            task.isDone = false
            coreDataManager.saveData()
        }
        
    }
    
    
    func setTask(task:Task){
        self.task = task
        if task.isDone{
            doneButton.setImage(#imageLiteral(resourceName: "Done"), for: .normal)
        }else{
            doneButton.setImage(#imageLiteral(resourceName: "UnDone"), for: .normal)
        }
    }
}
